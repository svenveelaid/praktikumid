package praktikum2;

import lib.TextIO;

public class GrupiSuurus {

	public static void main(String[] args) {

		int grupisuurus;
		int inimestearv;
		int gruppidearv;
		int ylejaak;

		System.out.println("Sisesta grupi suurus");
		grupisuurus = TextIO.getlnInt();

		System.out.println("Sisesta inimeste arv");
		inimestearv = TextIO.getlnInt();

		gruppidearv = inimestearv / grupisuurus;

		System.out.println("Saab teha " + gruppidearv + " gruppi");

		ylejaak = inimestearv % grupisuurus;

		System.out.println("�le j��b " + ylejaak + " inimest");

	}

}
